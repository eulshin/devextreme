const DB_NAME = 'testworkprofile'
const STORAGE_NAME = 'users'
let DB

export default {
  async getDB() {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open(DB_NAME);
      request.onerror = e => {
        console.log('Error opening db', e);
        reject('Error: Не могу отрыть базу данных');
      }
      request.onsuccess = e => {
        DB = e.target.result;
        resolve(DB);
      }
      request.onupgradeneeded = e => {
        let db = e.target.result;
        db.createObjectStore(STORAGE_NAME, { autoIncrement: true, keyPath: 'id' });
      }
    })
  },
  async getUser(id) {
    let db = await this.getDB();

    return new Promise(resolve => {
      let trans = db.transaction([STORAGE_NAME], 'readonly');
      const store = trans.objectStore(STORAGE_NAME);
      const user = store.get(id);
  
      trans.oncomplete = () => {
        resolve(user);
      }
    })
  },

  async saveUser(user) {
    let db = await this.getDB()
    return new Promise(resolve => {
      let trans = db.transaction([STORAGE_NAME], 'readwrite')

      let store = trans.objectStore(STORAGE_NAME)
      store.put(user)

      trans.oncomplete = () => {
        resolve()
      }
    })
  }
}