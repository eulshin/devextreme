export default [
  {
    text: "Home",
    path: "/home",
    icon: "home"
  },
  {
    text: "Examples",
    icon: "folder",
    items: [
      {
        text: "Profile",
        path: "/profile"
      },
      {
        text: "Tasks",
        path: "/tasks"
      }
    ]
  }, 
  {
    text: 'Test Work',
    path: '/test-work',
    icon: 'folder'
  }
  ];
